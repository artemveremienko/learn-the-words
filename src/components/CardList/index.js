import React from 'react'
import Card from '../Card';
import s from './CardList.module.scss';


const CardList = ({ item = [] }) => {

  return (
    <div className={s.root}>
      {
        item.map(({ name, descr }, index) => <Card key={index} item={index} name={name} descr={descr} />)
      }
    </div>
  )
}

export default CardList;