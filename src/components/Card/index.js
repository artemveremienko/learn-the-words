import React from 'react';
import cl from 'classnames';
import s from './Card.module.scss';
import { Html5Outlined, DashboardOutlined, CodeOutlined } from '@ant-design/icons';

class Card extends React.Component {
  state = {
    done: false,
  }

  handleCardClick = () => {
    this.setState({
      done: !this.state.done,
    })
  }

  render() {
    console.log(this.props);

    const { name, descr, item } = this.props;
    const { done } = this.state;

    const icons = [
      <Html5Outlined />,
      <DashboardOutlined />,
      <CodeOutlined />,
    ]

    return (
      <div
        className={cl(s.card, { [s.done]: done })}
        onClick={this.handleCardClick}
      >
        <div className={s.cardInner}>
          <div className={s.cardFront}>
            {icons[item]}
            {name}
          </div>
          <div className={s.cardBack}>
            {descr}
          </div>
        </div>
      </div >
    );
  }
}

export default Card;