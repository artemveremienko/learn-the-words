import React from 'react';
import HeaderBlock from './components/HeaderBlock';
import Header from './components/Header';
import Paragraph from './components/Paragraph';
import CardList from './components/CardList';

const stackList = [
  {
    name: 'HTML',
    descr: 'Hypertext Markup Language is the standard markup language for documents designed to be displayed in a web browser.'
  },
  {
    name: 'CSS',
    descr: 'Cascading Style Sheet is a simple mechanism for adding style (e.g., fonts, colors, spacing) to Web documents.'
  },
  {
    name: 'JS',
    descr: 'JavaScript is a lightweight, interpreted, or just-in-time compiled programming language with first-class functions.'
  },
];


const App = () => {
  return (
    <>
      <HeaderBlock>
        <Header>
          Front-end developers skills
        </Header>
        <Paragraph>
          Learn this technologies for start your way
        </Paragraph>
      </HeaderBlock>
      <CardList item={stackList} />
      <HeaderBlock hideBackground>
        <Header>
          Try it now!
        </Header>
        <Paragraph>
          It's awesome <span role="img" aria-label="emoji">🤓</span>
        </Paragraph>
      </HeaderBlock>
    </>
  )
}

export default App;
